// insert a single room (insertOne method) 

db.rooms.insertOne({
    roomName: "single",
    accomodates: 2,
    price: 1000,
    decription: "A simple room with all the basic necessities.",
    roomsAvailable: 10,
    isAvailable: false

})

// insert multiple rooms (insertMany method)

db.rooms.insertMany([{
    roomName: "double",
    accomodates: 3,
    price: 2000,
    decription: "A room fit for a small faily going on a vacation.",
    roomsAvailable: 5,
    isAvailable: false

},
{
    roomName: "queen",
    accomodates: 4,
    price: 4000,
    decription: "A room with a queen sized bed perfect for a simple getaway.",
    roomsAvailable: 15,
    isAvailable: false
}])

// use find method to search for a room with the name -double-

db.rooms.find({roomName: "double"})

// use the updateOne method to update the queen room and set the available rooms to 0

db.rooms.updateOne({roomName: "queen"},
{$set:{
 
    roomsAvailable: 0,

}
})

// use deleMany method rooms to delete all rooms that have 0 availability



db.rooms.deleteMany({roomsAvailable: 0})
// this deletes the 'queen' document because roomAvailable is 0