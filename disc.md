*CRUD operations*


----------------------------------
*CREATE - inserting documents*
----------------------------------

*SYNTAX*
    db.collectionName.InsertOne({})
    db.collectionName.Insert({})
    db.collectionName.InsertMany([{}, {}])

*Example*
    db.users.insert({
    firstname: "John",
    lastname: "Doe",
    age:21,
    contact:{
        phone: "09882345736",
        email: "jd@email.com"
        },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
})


*db.collectionName.InsertMany([{}, {}])*

*Example*
db.users.insertMany([{
    
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
        },
    courses: ["Python", "React", "PHP"],
    department: "none"
    
    },
    {
    
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
        phone: "87654321",
        email: "neilarmstrong@gmail.com"
        },
    courses: ["React", "RLaravel", "Sass"],
    department: "none"
  
  }])

-----------------------------------
*READ - Finding Documents*
-----------------------------------

*SYNTAX*
db.collectionName.Find({field: value})

*EXAMPLE*
db.users.find({firstName: "Neil"})

*If documents match the criteria, the document will return the match found.*

*Leaving the search criteria empty will retrieve all the available documents*

---------------------------------
*UPDATE - updating document*
*set is always with the update*
---------------------------------

*SYNTAX*
db.collection.updateOne({criteria}, {$set:{ } })

*EXAMPLES*
db.users.updateOne({
   firstName: "test"},
    {$set:{
        firstName: "Timmy",
        lastName: "Gingersnap",
        age: 34,
        contact: {
            phone: "222343453",
            email: "timmyG@mail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "Active"
    }})
*-------------------------------------*
    db.users.updateOne({ firstName: "Test" }, { $set: {
    
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345678",
        email: "billgates@gmail.com"
        },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations",
    status: "active"
    
    }})

*updateMany*
db.users.updateMany({ department: "none" }, { $set: {
    department: "HR"

    }})


*replaceOne - It can be used if replacing the whole document is necessary*

*SYNTAX*
db.collection.replaceOne({criteria}, { fields need to change})

*EXAMPLE*
db.users.replaceOne({firstName: "Bill"}, {
    firstName: "Bill",
    lastName: "Gates",
    age: 65,
    contact: {
        phone: "12345678",
        email:  "bill@gmail.com"
    },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations"
}
)


-----------------------------------
*DELETE - deleting document*
-deleteOne({})
-deleteMany({})
-----------------------------------

*SYNTAX - deleteOne: deletes one document*
db.collection.deleteOne({criteria})

*SYNTAX - deleteMany*
db.collection.deleteMany({criteria})

*SAMPLE - deleteMany: THIS DELETES ALL DOCUMENTS*
db.collection.deleteMany({ })



------------------------------------
*ADVANCE QUERY-ING*
------------------------------------

*Query an embedded document*

*-- FIND: EXAMPLE--*
db.users.find(  {
    
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
        }
    
    } )
------------------------------------------------
*WHEN FINDING a nested field (nested object. dot notation)*

db.users.find(  {
    
    "contact.email": "jh@email.com"
    
    } )
------------------------------------------------
*Query an Array (query with the exact elements in the array)*

db.users.find(  {
    
    courses: {["", ...]}
    
    } )

-----------------------------------------------
*query all element belongs to with a specific element, disregards also the order*

db.users.find(  {
    
    courses: {$all: ["React"]}
    
    } )
------------------------------------------------
*query an array of object*

*EXAMPLE*
db.users.insert({ nameArray: [{ nameA: "juan" }, { nameB: "tamad" }] })

*THEN FIND*
db.users.find({ nameArray: { nameA: "juan" } })


